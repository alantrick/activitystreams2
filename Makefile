.PHONY: coverage docs

coverage:
	pytest --cov=activitystreams2 tests/

docs:
	rm docs/api -rf
	sphinx-apidoc -fMT -o docs/api activitystreams2
	sphinx-build -a docs build/sphinx/html
