Changes
=======

0.5
---

* Rename to activitystreams2
* Add api documentation

0.4.1
-----

* Document upcoming name change
* Drop PBR and switch to using a package

0.4
---

* Add rudimentary support for extension properties

0.3
---

* Fix bug printing collections with only 1 item

0.2.0
-----

* Add support for ActivityPub extension

0.1.0
-----

* Initial version
