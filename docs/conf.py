# -- Django workaround
import os
import sys

# sys.path.append(os.path.dirname(__file__))

# -- Project information -----------------------------------------------------

project = "Activity Streams 2"
copyright = "2019, Alan Trick"
author = "Alan Trick"

extensions = ["sphinx.ext.doctest", "sphinx.ext.autodoc"]

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

source_suffix = ".rst"

master_doc = "index"

language = None

exclude_patterns = ["_build", "api/modules.rst"]

pygments_style = "sphinx"


# -- Options for HTML output -------------------------------------------------

html_theme = "sphinx_rtd_theme"
html_static_path = ["_static"]
# htmlhelp_basename = "DjangoVoxdoc"

# -- Options for LaTeX output ------------------------------------------------

latex_elements = {}

latex_documents = [
    (
        master_doc,
        "ActivityStreams2.tex",
        "Activity Streams 2 Documentation",
        "Alan Trick",
        "manual",
    )
]


# -- Options for manual page output ------------------------------------------

man_pages = [
    (master_doc, "activitystreams2", "Activity Streams 2 Documentation", [author], 1)
]


# -- Options for Texinfo output ----------------------------------------------

texinfo_documents = [
    (
        master_doc,
        "activitystreams2",
        "Activity Streams 2 Documentation",
        author,
        "activitystreams2",
        "One line description of project.",
        "Miscellaneous",
    )
]
